PREFIX = /usr/local

CC = gcc
CFLAGS = -lm

all: fetchy

fetchy:
	$(CC) $(CFLAGS) fetchy.c -o $@

clean:
	-rm -f fetchy

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f fetchy $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/fetchy

uninstall:
	-rm -f $(DESTDIR)$(PREFIX)/bin/fetchy

.PHONY: all install uninstall clean
