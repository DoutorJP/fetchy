#pragma GCC diagnostic ignored "-Wunused-function"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>

#define BUF_SIZE 20


void truncate_spaces_leading(char *str);
void truncate_spaces_trailing(char *str);
void read_line(char x);
int count_files(char *path);


static char *get_sys(char *sys, char *os_name);
static char *get_kernel(char *kern_name);
static char *get_uptime(char *uptime);
static char *get_RAM(char *ram_info);
static char *get_cpu(char *cpuname);
static char *get_gpu(char *gpu);
static int get_packages(char sys);



int main(int argc, char *argv[]){

	char sys;
	char *os_name = NULL, *uptime = NULL, *kern_name = NULL, *ram_info = NULL, *cpu_name = NULL, *gpu = NULL;
	char *os = malloc(BUF_SIZE), *cache_path = malloc(BUF_SIZE * 2);

	snprintf(cache_path, BUF_SIZE * 2, "%s/.local/share/fetchy.cache", getenv("HOME"));
	FILE *FCache = NULL;

	if( (FCache = fopen(cache_path, "r")) ){
		cpu_name = malloc(BUF_SIZE * 4);
		gpu = malloc(BUF_SIZE * 4);

		char *line = malloc(BUF_SIZE * 4);

		fgets(line, BUF_SIZE * 4, FCache);
		snprintf(cpu_name, BUF_SIZE * 4, "%s", strchr(line, ':') +2); 
		truncate_spaces_trailing(cpu_name);

		fgets(line, BUF_SIZE * 4, FCache);
		snprintf(gpu, BUF_SIZE * 4, "%s", strchr(line, ':') +2); 
		truncate_spaces_trailing(gpu);

		fclose(FCache);
		free(line);
	}

	else {
		
		FCache = fopen(cache_path, "w");
		fprintf(FCache, "CPU : %s\n", get_cpu(cpu_name));
		fprintf(FCache, "GPU : %s\n", get_gpu(gpu));
		fclose(FCache);

		printf("\n\n\aFILE CACHING DONE!! \n\nValues returned : \nCPU -> %s\nGPU -> %s\n\n Caching occurs only once, every subsequent execution will run program normally. Enjoy!\n\n", get_cpu(cpu_name), get_gpu(gpu));

		exit(0);
	}
	




	os = get_sys(&sys, os_name);

	switch(sys){

		case 'a':

			printf("\n");
			printf("\x1b[1m         .          \x1b[36mOS\x1b[0m    ->   %s\n", os);
			printf("\x1b[1m        / \\         \x1b[36mKRNL\x1b[0m  ->   %s\n", get_kernel(kern_name));
			printf("\x1b[1m       /   \\        \x1b[36mUPT\x1b[0m   ->   %s\n", get_uptime(uptime));
			printf("\x1b[1m      /^.   \\       \x1b[36mPKGS\x1b[0m  ->   %d\n", get_packages(sys));
			printf("\x1b[1m     /  .-.  \\      \x1b[36m\x1b[0m     \n");
			printf("\x1b[1m    /  (   ) _\\     \x1b[36mCPU\x1b[0m   ->   %s\n", cpu_name);
			printf("\x1b[1m   / _.~   ~._^\\    \x1b[36mGPU\x1b[0m   ->   %s\n", gpu);
			printf("\x1b[1m  /.^         ^.\\   \x1b[36mRAM\x1b[0m   ->   %s\n", get_RAM(ram_info));
			printf("\n\n");
		break;


		case 'u':

			printf("\n");
			printf("\x1b[1m                .-.		\x1b[36mOS\x1b[0m    ->   %s\n", os);
			printf("\x1b[1m          .-'``(|||)		\x1b[36mKERNEL\x1b[0m  ->   %s\n", get_kernel(kern_name));
			printf("\x1b[1m       ,`\\ \\    `-`.		\x1b[36mUPTIME\x1b[0m   ->   %s\n", get_uptime(uptime));
			printf("\x1b[1m      /   \\ '``-.   `		\x1b[36mPACKAGES\x1b[0m  ->   *W.I.P.*\n"); //%d\n", get_packages(sys));
			printf("\x1b[1m    .-.  ,       `___:		\x1b[36m\x1b[0m     \n");
			printf("\x1b[1m   (:::) :        ___		\x1b[36mCPU\x1b[0m   ->   %s\n", cpu_name);
			printf("\x1b[1m    `-`  `       ,   :		\x1b[36mGPU\x1b[0m   ->   %s\n", gpu);
			printf("\x1b[1m      \\   / ,..-`   ,		\x1b[36mRAM\x1b[0m   ->   %s\n", get_RAM(ram_info));
			printf("\x1b[1m       `./ /    .-.`			\x1b[36m\x1b[0m        \n");
			printf("\x1b[1m          `-..-(   )  		\x1b[36m\x1b[0m        \n");
			printf("\x1b[1m                `-`			\x1b[36m\x1b[0m        \n");
			printf("\n\n");
		break;

		default:
			printf("\n\n ERROR : Unsupported system\n\n");
			exit(-1);
	}

	

	free(os_name);
	free(uptime);
	free(ram_info);
	free(kern_name);
	free(cpu_name);
	free(gpu);
	free(cache_path);
	return 0;
}




void read_line(char x){
    int c;
    while (( c = getchar()) != x && c != EOF)  { }
}



    // taken from https://github.com/ss7m/paleofetch
void truncate_spaces_leading(char *str){
    int src = 0, dst = 0;
    while(*(str + dst) == ' ') dst++;

    while(*(str + dst) != '\0') {
        *(str + src) = *(str + dst);
        if(*(str + (dst++)) == ' ')
            while(*(str + dst) == ' ') dst++;

        src++;
    }

    *(str + src) = '\0';
}


void truncate_spaces_trailing(char *str){
    int index, i = -1;


    i = 0;
    while(str[i] != '\0'){
        if(str[i] != ' ' && str[i] != '\t' && str[i] != '\n'){
            index= i;
		}
        i++;
    }


    str[index + 1] = '\0';
}


static char *get_sys(char *sys, char *os_name){
	os_name = malloc(BUF_SIZE);
	char *name = malloc(BUF_SIZE);
	FILE *sysName = fopen("/etc/issue", "rt");
	
	fscanf(sysName, "%s ", name);
	fclose(sysName);
	truncate_spaces_leading(name);

	snprintf(os_name, BUF_SIZE, "%s Linux", name);
	
	if ( !strcmp( name, "Arch")) *sys = 'a';
	if ( !strcmp( name, "Ubuntu"))	*sys = 'u';
		
	free(name);
	return os_name;
}


static char *get_cpu(char *cpu_name){
	char *line = malloc(BUF_SIZE * 4);
	cpu_name = malloc(BUF_SIZE * 4);
	FILE *cpu = fopen("/proc/cpuinfo", "rt");
	
	for(int i = 0; i < 5; i++)
		fgets(line, BUF_SIZE * 4, cpu);

	snprintf(cpu_name, BUF_SIZE * 4, "%s", strchr(line, ':') +2); 
	truncate_spaces_trailing(cpu_name);
	
	fclose(cpu);
	free(line);

	return cpu_name;
}


static char *get_gpu(char *gpu){

	gpu = malloc(BUF_SIZE * 3);
	FILE *gpuName = popen("lspci -v | grep VGA -m 1 | awk -F'[' '{ print $2 }' | awk  -F']' '{ print $1 }' ", "r");

	fscanf(gpuName, "%[^\n]%s", gpu);

	truncate_spaces_leading(gpu);
	truncate_spaces_trailing(gpu);

	fclose(gpuName);


    if(strstr(gpu, "VGA"))
        strcpy(gpu, "CPU's integrated graphics");
	
	return gpu;
}


static char *get_kernel(char *kern_name){
	kern_name = malloc(BUF_SIZE);
	char *kernel = malloc(BUF_SIZE);
	FILE *kInfo = fopen("/proc/version", "rt");

	fscanf(kInfo, "Linux version %s ", kernel);
	fclose(kInfo);

	truncate_spaces_leading(kernel);
	snprintf(kern_name, BUF_SIZE + 5, "Linux %s", kernel);
	free(kernel);

	return kern_name;
}


static char *get_uptime(char *uptime){
	int sec, hr, min;
	FILE *FUp = fopen("/proc/uptime", "rt");
	uptime = malloc(BUF_SIZE);

	fscanf(FUp, "%d", &sec);
	fclose(FUp);

	hr = (sec/60/60%24);
	min = (sec/60%60);

	snprintf(uptime, BUF_SIZE, "%dh, %dmin", hr, min);
	return uptime;
}


static int get_packages(char sys){

	DIR *packageDir;
	int fileCount = 0;
	struct dirent * entry;

	switch (sys){
		case 'a':
			packageDir = opendir("/var/lib/pacman/local");
			break;
		case 'u':
			
			break;
		default:
			printf("\n\n\aUnable to find package list\n\n");
	}



	while ((entry = readdir(packageDir)) != NULL) {
		if (entry->d_type == DT_DIR) {
			fileCount++;
		}
	}
	closedir(packageDir);



	return fileCount;

}


static char *get_RAM(char *ram_info){
	int bar_percent;
	float total, free_mem, used;
	float real_percent;
	ram_info = malloc(BUF_SIZE * 3);
	char *line = malloc(BUF_SIZE * 3);
	char bar[] = "[----------]";

	FILE *RAM = fopen("/proc/meminfo", "rt");

	

	fgets(line, BUF_SIZE * 3, RAM);
	truncate_spaces_leading(line);
	sscanf(line, " MemTotal: %f", &total);

    fgets(line, BUF_SIZE * 3, RAM);

	fgets(line, BUF_SIZE * 3, RAM);
	truncate_spaces_leading(line);
	sscanf(line, " MemAvailable: %f", &free_mem);

	fclose(RAM);

	used = total - free_mem;
	real_percent = (used / total) * 100;
	bar_percent = (real_percent / 10 + 0.35);

	for( int i = 1; i <= bar_percent; i++){
		bar[i] = '*';
	}

	snprintf(ram_info, BUF_SIZE * 3, "%.2f GB of %.2f GB,  %s -> %.2f %%", used / 1000000, total / 1000000, bar, real_percent);

	free(line);
	return ram_info;
}


