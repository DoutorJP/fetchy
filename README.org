#+TITLE: Fetchy

Currently supported : Arch, Artix and Ubuntu. (more to come)


* Table of Contents :toc:
- [[#what-is-fetchy][What is fetchy?]]
- [[#install][Install]]
  - [[#source][Source]]
- [[#run][Run]]

* What is fetchy?

#+CAPTION: fetchy Screenshot
#+ATTR_HTML: :alt fetchy Screenshot :title fetchy Screenshot :align left
[[https://gitlab.com/vojjvoda/fetchy/-/raw/master/arch.png]]


fetchy - simple cli system information tool written in C. 


* Install

** AUR

#+begin_src
yay -S fetchy-git
#+end_src

** Source

#+begin_src
git clone https://gitlab.com/vojjvoda/fetchy
cd fetchy
sudo make install
#+end_src

* Run 

#+begin_src 
fetchy
#+end_src

